'use strict';

angular.module('bookerappApp')
    .controller('LogoutController', function (Auth) {
        Auth.logout();
    });
