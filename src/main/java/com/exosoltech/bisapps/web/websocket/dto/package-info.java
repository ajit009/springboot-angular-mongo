/**
 * Data Access Objects used by WebSocket services.
 */
package com.exosoltech.bisapps.web.websocket.dto;
