/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.exosoltech.bisapps.web.rest.dto;
