/* globals $ */
'use strict';

angular.module('bookerappApp')
    .directive('bookerappAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
