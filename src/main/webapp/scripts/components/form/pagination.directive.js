/* globals $ */
'use strict';

angular.module('bookerappApp')
    .directive('bookerappAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
